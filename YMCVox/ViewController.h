#import <CoreMotion/CoreMotion.h>
#import "MHAudioBufferPlayer.h"
#import "Synth.h"

#import <UIKit/UIKit.h>


@class MHAudioBufferPlayer;

@interface ViewController : UIViewController
{
   MHAudioBufferPlayer *_player;
   Synth *_synth;
   NSLock *_synthLock;
}


@end

