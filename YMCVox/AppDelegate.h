//
//  AppDelegate.h
//  YMCVox
//
//  Created by Mikhail Baynov on 26/11/2016.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

